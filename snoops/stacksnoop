#! /usr/bin/python3

from __future__ import print_function
from bcc import BPF
import ctypes
import time

prog = """
#include <uapi/linux/ptrace.h>
#include <linux/sched.h>

struct data_t {
    u64 stack_id;
    u32 pid;
    char comm[TASK_COMM_LEN];
};

BPF_STACK_TRACE(stack_traces, 128);
BPF_PERF_OUTPUT(events);

// 必须要将 stack_id 和 ctx 绑定起来，这个是 kprobe 提供的 stacktrace 的要求
// 通过 perf_submit 传递数据
void trace_stack(struct pt_regs *ctx){
    u32 pid = bpf_get_current_pid_tgid();

    struct data_t data = {};
    data.stack_id = stack_traces.get_stackid(ctx, 0);
    data.pid = pid;
    bpf_get_current_comm(&data.comm, sizeof(data.comm));
    events.perf_submit(ctx, &data, sizeof(data));
}
"""

# 可配置参数
function = "open_exec"
offset = 0
start_ts = time.time()

b = BPF(text=prog)
b.attach_kprobe(event=function, fn_name="trace_stack")

if b.num_open_kprobes() == 0:
    print("function not found,exiting...")
    exit()

stack_traces = b.get_table("stack_traces")

print("%-18s %-12s %-6s %-3s %s" %
      ("TIME(s)", "COMM", "PID", "CPU", "FUNCTION"))


class Data(ctypes.Structure):
    _fields_ = [
        ("stack_id", ctypes.c_ulonglong),
        ("pid", ctypes.c_uint),
        ("comm", ctypes.c_char * 16),
    ]


def print_event(cpu, data, size):
    event = ctypes.cast(data, ctypes.POINTER(Data)).contents

    ts = time.time() - start_ts

    print("%-18.9f %-12.12s %-6d %-3d %s" %
          (ts, event.comm.decode(), event.pid, cpu, function))
    for addr in stack_traces.walk(event.stack_id):
        sym = b.ksym(addr, show_offset=offset)
        print("\t%s" % sym)
    print()


b["events"].open_perf_buffer(print_event)

while True:
    try:
        b.perf_buffer_poll()
    except KeyboardInterrupt:
        exit()
    print()
