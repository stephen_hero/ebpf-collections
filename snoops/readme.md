# ebpf with other traceing mechanisims 

1. kprobe && kretprobe  -- kernel dynamic probe  
2. uprobe && USDT       -- user dynamic | static probe
3. tracepoint           -- kernel static probe 
4. perf event           -- system hardware pmu
5. xdp                  -- xExpress data path, advanced topic
